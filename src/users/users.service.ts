import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private userRrpository: Repository<User>,
  ) {}
  create(createUserDto: CreateUserDto) {
    return this.userRrpository.save(createUserDto);
  }

  findAll() {
    return this.userRrpository.find();
  }

  findOne(id: number) {
    return this.userRrpository.findOne({ where: { id } });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.userRrpository.findOneBy({ id });
    if (!user) {
      throw new NotFoundException();
    }
    const updatedUser = { ...user, ...updateUserDto };
    return this.userRrpository.save(updatedUser);
  }

  async remove(id: number) {
    const user = await this.userRrpository.findOneBy({ id });
    if (!user) {
      throw new NotFoundException();
    }
    return this.userRrpository.softRemove(user);
  }
}
